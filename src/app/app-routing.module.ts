import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { Component1 } from './components/component1/component1.component';
import { Component2 } from './components/component2/component2.component';

const ROUTES: Routes = 
[
  {path: "", component: AppComponent},
  {path: "component1", component: Component1},
  {path: "component2", component: Component2},
  {path: "plugin", loadChildren: () => import('./components/plugin-component/plugin.module').then(m => m.PluginModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
