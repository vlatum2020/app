import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Component1 } from './components/component1/component1.component';
import { Component2 } from './components/component2/component2.component';
import { PluginModule } from './components/plugin-component/plugin.module';

@NgModule({
  declarations: [
    AppComponent,
    Component1,
    Component2
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PluginModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
