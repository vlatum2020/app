import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Component2 } from './component2.component';

describe('Component2Component', () => {
  let component: Component2;
  let fixture: ComponentFixture<Component2>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Component2 ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Component2);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
