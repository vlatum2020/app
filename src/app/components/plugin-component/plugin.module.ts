import { NgModule, CompilerOptions } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import {  RouterModule, Routes } from "@angular/router";
import { PluginComponent } from "./plugin-component.component";


import { System } from "systemjs"
import { Router } from "@angular/router";
export declare var SystemJS: System;

import * as angularCore from "@angular/core";
import * as angularCommon from "@angular/common";
import * as angularRouter from "@angular/router";

import * as angularCommonHttp from "@angular/common/http";
import * as angularForms from "@angular/forms";
import * as angularAnimations from "@angular/animations";
import * as angularPlatformBrowser from "@angular/platform-browser";
import * as angularPlatformBrowserDynamic from "@angular/platform-browser-dynamic";
import * as httpClient from "@angular/common/http";
import * as rxjs from "rxjs";
import { AppModule } from "src/app/app.module";

SystemJS.set("@angular/core", SystemJS.newModule(angularCore));
SystemJS.set("@angular/common", SystemJS.newModule(angularCommon));
SystemJS.set("@angular/common/http", SystemJS.newModule(angularCommonHttp));
SystemJS.set("@angular/common/http", SystemJS.newModule(httpClient));
SystemJS.set("@angular/animations", SystemJS.newModule(angularAnimations));
SystemJS.set("@angular/forms", SystemJS.newModule(angularForms));
SystemJS.set("rxjs", SystemJS.newModule(rxjs));
SystemJS.set(
	"@angular/platform-browser",
	SystemJS.newModule(angularPlatformBrowser)
);
SystemJS.set(
	"@angular/platform-browser-dynamic",
	SystemJS.newModule(angularPlatformBrowserDynamic)
);
SystemJS.set(
	"@angular/router",
	SystemJS.newModule(angularRouter)
);

SystemJS.config({ meta: { "*": { authorization: true } } });


var WidgetRoutes = JSON.parse(localStorage.getItem("pluginRoutes"));
var Widgets = JSON.parse(localStorage.getItem("plugins"));
const routes: Routes = 
[
];
console.log(WidgetRoutes, Widgets);
if (WidgetRoutes) {
	for (let i = 0; i < WidgetRoutes.length; i++) {
		routes.push({ path: WidgetRoutes[i],  loadChildren:  async () => 
			{
				debugger
				const module = await SystemJS.import(`assets/plagin1-0.0.1.bundle.js?a=${Date.now()}`);
				return module['Plugin1AppModule'];
			} });
	}
}


@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		RouterModule.forChild(routes)
	],
	declarations: [PluginComponent],
	providers: [
		
	],
})
export class PluginModule {}
