System.register(['@angular/core', '@angular/router', '@angular/common'], (function (exports) {
    'use strict';
    var Component, NgModule, RouterModule, CommonModule;
    return {
        setters: [function (module) {
            Component = module.Component;
            NgModule = module.NgModule;
        }, function (module) {
            RouterModule = module.RouterModule;
        }, function (module) {
            CommonModule = module.CommonModule;
        }],
        execute: (function () {

            /*! *****************************************************************************
            Copyright (c) Microsoft Corporation.

            Permission to use, copy, modify, and/or distribute this software for any
            purpose with or without fee is hereby granted.

            THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
            REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
            AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
            INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
            LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
            OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
            PERFORMANCE OF THIS SOFTWARE.
            ***************************************************************************** */

            function __decorate(decorators, target, key, desc) {
                var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
                else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            }

            let AppComponent = class AppComponent {
                constructor() {
                    this.title = 'plagin1';
                }
            };
            AppComponent = __decorate([
                Component({
                    selector: 'app-root',
                    template: `
  <div style="background-color: aqua;">
    <p> PLUGIN </p>
    <button routerLink="one"> plugin sub</button>
    <button routerLink="two"> plugin sub 2</button>
    <router-outlet></router-outlet>
  </div>
  `,
                    styles: [``]
                })
            ], AppComponent);

            let SubComponent = class SubComponent {
                constructor() { }
                ngOnInit() {
                }
            };
            SubComponent = __decorate([
                Component({
                    selector: 'app-sub',
                    template: `<p>sub works!</p>`,
                    styles: [''],
                    providers: []
                })
            ], SubComponent);

            let Sub2Component = class Sub2Component {
                constructor() { }
                ngOnInit() {
                }
            };
            Sub2Component = __decorate([
                Component({
                    selector: 'app-sub2',
                    template: `<p>sub 2 works!</p>`,
                    styles: ['']
                })
            ], Sub2Component);

            const PLUGIN_ROUTE = [
                { path: "plg", component: AppComponent,
                    children: [
                        { path: "one", component: SubComponent },
                        { path: "two", component: Sub2Component },
                    ]
                }
            ];
            let AppRoutingModule = class AppRoutingModule {
            };
            AppRoutingModule = __decorate([
                NgModule({
                    imports: [RouterModule.forChild(PLUGIN_ROUTE)],
                    exports: [RouterModule],
                    providers: []
                })
            ], AppRoutingModule);

            let Plugin1AppModule = exports('Plugin1AppModule', class Plugin1AppModule {
            });
            exports('Plugin1AppModule', Plugin1AppModule = __decorate([
                NgModule({
                    declarations: [
                        AppComponent,
                        SubComponent,
                        Sub2Component
                    ],
                    imports: [
                        CommonModule,
                        AppRoutingModule
                    ],
                    providers: [],
                    bootstrap: [AppComponent]
                })
            ], Plugin1AppModule));

        })
    };
}));
